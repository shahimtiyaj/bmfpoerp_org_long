package net.qsoft.brac.bmfpo.data;

import net.qsoft.brac.bmfpo.P8;

import java.util.Date;

public class VO {
	private static final String TAG = VO.class.getSimpleName();

	private Long _OrgNo;
	private String _OrgName;
	private String _OrgCategory;
	private Integer _MemSexID;
	private Integer _SavColcOption;
	private Integer _LoanColcOption;
	private Date _SavColcDate;
	private Date _LoanColcDate;
	private Date _TargetDate;
	private Integer _Period;

	/**
	 * @param _OrgNo
	 * @param _OrgName
	 * @param _OrgCategory
	 * @param _MemSexID
	 * @param _SavColcOption
	 * @param _LoanColcOption
	 * @param _SavColcDate
	 * @param _LoanColcDate
	 */


	public VO(Long _OrgNo, String _OrgName, String _OrgCategory,
			  Integer _MemSexID, Integer _SavColcOption, Integer _LoanColcOption,
			  Date _SavColcDate, Date _LoanColcDate, Date _TargetDate) {
		super();
		this._OrgNo = _OrgNo;
		this._OrgName = _OrgName;
		this._OrgCategory = _OrgCategory;
		this._MemSexID = _MemSexID;
		this._SavColcOption = _SavColcOption;
		this._LoanColcOption = _LoanColcOption;
		this._SavColcDate = _SavColcDate;
		this._LoanColcDate = _LoanColcDate;
		this._TargetDate = _TargetDate;
		this._Period = 0;
/*
		if(this._LoanColcDate.after(new Date())) {
			// need to calculate new target date
			// find the period type first
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			switch(this._LoanColcOption) {
			case 0:
				break;

			case 1:
				// Weekly
				//while(cal.getTime() < )

				break;

			case 2:
				// Monthly

			}
		}
		else {
			this._TargetDate=this._LoanColcDate;
			this._Period=0;
		}		*/
	}


	public VO()
	{

	}

	/**
	 * @return the _OrgNo
	 */
	public final Long get_OrgNo() {
		return _OrgNo;
	}
	/**
	 * @return the _OrgName
	 */
	public final String get_OrgName() {
		return _OrgName;
	}
	/**
	 * @return the _OrgCategory
	 */
	public final String get_OrgCategory() {
		return _OrgCategory;
	}
	/**
	 * @return the _MemSexID
	 */
	public final Integer get_MemSexID() {
		return _MemSexID;
	}
	/**
	 * @return the _SavColcOption
	 */
	public final Integer get_SavColcOption() {
		return _SavColcOption;
	}
	/**
	 * @return the _LoanColcOption
	 */
	public final Integer get_LoanColcOption() {
		return _LoanColcOption;
	}
	/**
	 * @return the _SavColcDate
	 */
	public final Date get_SavColcDate() {
		return _SavColcDate;
	}
	/**
	 * @return the _LoanColcDate
	 */
	public final Date get_LoanColcDate() {
		return _LoanColcDate;
	}

	public final Date get_TargetDate() {
		return _TargetDate;
	}

	public final Integer get_Period() {
		return _Period;
	}

/*	public final void set_Today(Date newDate) {
		this._Today = newDate;
	}

	public final Date get_Today() {
		return _Today;
	} */

	public final boolean isTodaysVO() {
//		Log.d(TAG, get_OrgNo()+ " Target: " + get_TargetDate());
//		Log.d(TAG, get_OrgNo()+ " Today: " + P8.ToDay());
		return  P8.FormatDate(get_TargetDate(), "yyyyMMdd").equals(P8.FormatDate(P8.ToDay(), "yyyyMMdd"));
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return get_OrgNo() + " - " + get_OrgName();
	}

}
