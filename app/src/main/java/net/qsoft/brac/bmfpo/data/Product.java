package net.qsoft.brac.bmfpo.data;

public class Product {
	String _ProductType;
	String _ProductNo;
	String _ProductName;
	Double _IntRate;
	Integer _IntCalcMethod;
	Integer _Status;

	/**
	 * @param _ProductType
	 * @param _ProductNo
	 * @param _ProductName
	 * @param _IntRate
	 * @param _IntCalcMethod
	 * @param _Status
	 */
	public Product(String _ProductType, String _ProductNo, String _ProductName,
			Double _IntRate, Integer _IntCalcMethod, Integer _Status) {
		super();
		this._ProductType = _ProductType;
		this._ProductNo = _ProductNo;
		this._ProductName = _ProductName;
		this._IntRate = _IntRate;
		this._IntCalcMethod = _IntCalcMethod;
		this._Status = _Status;
	}

	/**
	 * @return the _ProductType
	 */
	public final String get_ProductType() {
		return _ProductType;
	}

	/**
	 * @return the _ProductNo
	 */
	public final String get_ProductNo() {
		return _ProductNo;
	}

	/**
	 * @return the _ProductName
	 */
	public final String get_ProductName() {
		return _ProductName;
	}

	/**
	 * @return the _IntRate
	 */
	public final Double get_IntRate() {
		return _IntRate;
	}

	/**
	 * @return the _IntCalcMethod
	 */
	public final Integer get_IntCalcMethod() {
		return _IntCalcMethod;
	}

	/**
	 * @return the _Status
	 */
	public final Integer get_Status() {
		return _Status;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ((get_ProductType()=="L") ? "Loan product ": "Savings product") 
				+ " " + get_ProductNo()
				+ " - " + get_ProductName();
	}

}
