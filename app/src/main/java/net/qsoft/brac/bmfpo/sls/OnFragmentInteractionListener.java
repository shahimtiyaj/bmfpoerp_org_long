package net.qsoft.brac.bmfpo.sls;

/**
 * Created by zferdaus on 10/25/2017.
 */

public interface OnFragmentInteractionListener {
    void onFragmentInteraction(Integer index, Long vno, Long mno, String mname);
}
