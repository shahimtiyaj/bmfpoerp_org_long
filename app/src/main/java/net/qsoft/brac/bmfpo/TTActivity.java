package net.qsoft.brac.bmfpo;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import net.qsoft.brac.bmfpo.data.DAO;
import net.qsoft.brac.bmfpo.data.DAO.DBoardItem;

import java.util.ArrayList;
import java.util.HashMap;

//import net.qsoft.brac.bmfpo.data.DAO;

public class TTActivity extends SSActivity {
    private static final String TAG = TTActivity.class.getSimpleName();

    ListView lv = null;
    ArrayList<HashMap<String, String>> items = null;
    Long vono;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tt);
//		if(VERSION.SDK_INT >= VERSION_CODES.HONEYCOMB) {
//			getActionBar().setDisplayHomeAsUpEnabled(true);
//		}
        lv = (ListView) findViewById(R.id.lstView);
        if (getIntent().hasExtra(P8.VONO)) {
            vono = getIntent().getExtras().getLong(P8.VONO,0);
            Log.d(TAG, "In has extra " + vono);
            setTitle("VO Summary VO No.: " + vono);
        } else {
            if (VERSION.SDK_INT >= VERSION_CODES.HONEYCOMB) {
                getActionBar().setDisplayHomeAsUpEnabled(true);
            }
        }
        lv.setOnItemClickListener(new OnItemClickListener() {
            @SuppressWarnings("unchecked")
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                try {
                    DBoardItem item = DBoardItem.valueOf(((HashMap<String, String>) lv.getItemAtPosition(position)).get("Id"));
                    startAction(item);
                } catch (Exception ex) {
                    // TODO Auto-generated catch block
                    //e.printStackTrace();
                }
                //Toast.makeText(getBaseContext(), item, Toast.LENGTH_LONG).show();
            }
        });

        createDashBoard(vono);
    }

    private void createDashBoard(Long von) {
        Log.d(TAG, "VO No.: " + (von == null ? "" : von));
        DAO da = new DAO(this);
        da.open();
        items = da.getDashboardItems(von);
        da.close();

        DBoardAdapter adapter = new DBoardAdapter(this, R.layout.dashboard_row, items);
        lv.setAdapter(adapter);
    }

    /* (non-Javadoc)
     * @see android.app.Activity#onNewIntent(android.content.Intent)
     */
    @Override
    protected void onNewIntent(Intent intent) {
        // TODO Auto-generated method stub
        super.onNewIntent(intent);
        vono = intent.getExtras().getLong(P8.VONO);
        createDashBoard(vono);
    }

    private void startAction(DBoardItem itm) {
        switch (itm) {
            case VOS:
                startActivity(new Intent(this, VOActivity.class));
                break;

            case MEMBERS:
            case BORROWERS:
            case CURBORROWERS:
            case LOANS:
            case CURLOANS:
            case L1LOANS:
            case L2LOANS:
            case N1LOANS:
            case N2LOANS:
            case OUTLOANS:
            case OVERLOANS:
            case SAVE_TARGET:
            case SAVE_YET_COLLECTED:
            case REPAY_TARGET:
            case REPAY_YET_COLLECTED:

                Intent it = new Intent(this, ClrMemberActivity.class);
                it.putExtra(P8.DBITEMS, itm.name());
                if (vono != null)
                    it.putExtra(P8.VONO, vono);
                startActivity(it);
                break;

            case PO_CASH_IN_HAND:
                Intent itt = new Intent(this, CListActivity.class);
                if (vono != null)
                    itt.putExtra(P8.VONO, vono);
                startActivity(itt);
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // This ID represents the Home or Up button. In the case of this
                // activity, the Up button is shown. Use NavUtils to allow users
                // to navigate up one level in the application structure. For
                // more details, see the Navigation pattern on Android Design:
                // http://developer.android.com/design/patterns/navigation.html#up-vs-back
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onCancel(View view) {
        // Back
        finish();
    }

    /* (non-Javadoc)
     * @see android.app.Activity#onResume()
     */
    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        createDashBoard(vono);
    }

/*	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
	      super.onCreateContextMenu(menu, v, menuInfo);
	      MenuInflater inflater = getMenuInflater();
	      inflater.inflate(R.menu.dboard_menu, menu);
	} */

    /* (non-Javadoc)
     * @see android.app.Activity#onContextItemSelected(android.view.MenuItem)
     */
/*	@SuppressWarnings("unchecked")
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		Intent it;
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
	    HashMap<String, String>  temp = (HashMap<String, String>) lv.getItemAtPosition(info.position);
	    String _desc = temp.get("Desc");
	      
	    switch(item.getItemId()) {
	    case R.id.menu_vo_members:
	    	
            Toast.makeText(getBaseContext(), "VO Members: " + _desc, Toast.LENGTH_LONG).show();
	    	return true;
	    	  
	    case R.id.menu_vo_summary:
	    	
	    	Toast.makeText(getBaseContext(), "VO Summary: " + _desc, Toast.LENGTH_LONG).show();
	    	return true;
	    }
	    
		return super.onContextItemSelected(item);
	} */

}
