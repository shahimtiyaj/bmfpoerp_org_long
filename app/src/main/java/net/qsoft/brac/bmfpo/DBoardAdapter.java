package net.qsoft.brac.bmfpo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

public class DBoardAdapter extends ArrayAdapter<HashMap<String, String>> {
	// View lookup cache
    private static class ViewHolder {
        TextView dsc;
        TextView num;
        TextView amt;
    }

	public DBoardAdapter(Context context, int resource, List<HashMap<String, String>> rows) {
		super(context, resource, rows);
		// TODO Auto-generated constructor stub
	}

	 @Override
	    public View getView(int position, View convertView, ViewGroup parent) {
	       // Get the data item for this position
	       HashMap<String, String> row = getItem(position);    
	       // Check if an existing view is being reused, otherwise inflate the view
	       ViewHolder viewHolder; // view lookup cache stored in tag
	       if (convertView == null) {
	          viewHolder = new ViewHolder();
	          LayoutInflater inflater = LayoutInflater.from(getContext());
	          convertView = inflater.inflate(R.layout.dashboard_row, parent, false);
	          viewHolder.dsc = (TextView) convertView.findViewById(R.id.textDesc);
	          viewHolder.num = (TextView) convertView.findViewById(R.id.textTarget);
	          viewHolder.amt = (TextView) convertView.findViewById(R.id.textYTR);
	          convertView.setTag(viewHolder);
	       } else {
	           viewHolder = (ViewHolder) convertView.getTag();
	       }
//	       if(position%2==1)
//	    	   convertView.setBackgroundColor(Color.CYAN);
//	       else
//	    	   convertView.setBackgroundColor(Color.TRANSPARENT);
	       
	       // Populate the data into the template view using the data object
	       viewHolder.dsc.setText(row.get("Desc"));
	       viewHolder.num.setText(row.get("Num"));
	       viewHolder.amt.setText(row.get("Amt"));
	       // Return the completed view to render on screen
	       return convertView;
	   }	
	
}
