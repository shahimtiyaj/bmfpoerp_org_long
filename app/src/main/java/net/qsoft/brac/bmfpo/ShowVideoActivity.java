package net.qsoft.brac.bmfpo;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.MediaController;
import android.widget.VideoView;

public class ShowVideoActivity extends SSActivity {
	private static final String TAG = ShowVideoActivity.class.getSimpleName();
	
	// Declare variables
	ProgressDialog pDialog;
	VideoView videoview;
	private int position = 0;
	MediaController mediacontroller;
	
	// Insert your Video URL
	String VideoURL = "";
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_show_video);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
		
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(App.getContext());
		VideoURL = prefs.getString(P8.PREF_VIDEO_URL,
				Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES) + "/videotutorial.mp4");
//		if(VideoURL.trim().length()==0)
//			VideoURL = "https://youtu.be/uas0VVf6IP0";
		
		Log.d(TAG, "Video URL: " + VideoURL);
		
		videoview = (VideoView) findViewById(R.id.VideoView);
//		pDialog = new ProgressDialog(this);
//		pDialog.setMessage("Buffering...");
//		pDialog.setIndeterminate(false);
//		pDialog.setCancelable(false);
//		// Show progressbar
//		pDialog.show();
		
		try {
			// Start the MediaController
			  // Set the media controller buttons
	        if (mediacontroller == null) {
				mediacontroller = new MediaController(ShowVideoActivity.this);
				mediacontroller.setAnchorView(videoview);
				videoview.setMediaController(mediacontroller);
	        }
			// Get the URL from String VideoURL
			Uri video = Uri.parse(VideoURL);
			
			videoview.setVideoURI(video);

		
		} catch (Exception e) {
			Log.e(TAG, "Error: " + e.getMessage());
//			e.printStackTrace();
		}

		videoview.requestFocus();
	      // When the video file ready for playback.
        videoview.setOnPreparedListener(new OnPreparedListener() {
 
            public void onPrepared(MediaPlayer mediaPlayer) {
 
 
                videoview.seekTo(position);
                if (position == 0) {
                    videoview.start();
                }
 
                // When video Screen change size.
                mediaPlayer.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                    @Override
                    public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
 
                        // Re-Set the videoView that acts as the anchor for the MediaController
                        mediacontroller.setAnchorView(videoview);
                    }
                });
            }
        });
        
  
     
//		videoview.setOnPreparedListener(new OnPreparedListener() {
//			// Close the progress bar and play the video
//			public void onPrepared(MediaPlayer mp) {
////				pDialog.dismiss();
//				videoview.start();
//			}
//		});
		
/*		videoview.setOnErrorListener(new OnErrorListener() {
			@Override
			public boolean onError(MediaPlayer mp, int what, int extra) {
				// TODO Auto-generated method stub
				Toast.makeText(ShowVideoActivity.this, "*** Error ***", Toast.LENGTH_SHORT);
				ShowVideoActivity.this.finish();
				return false;
			}
		});
		
		videoview.setOnCompletionListener(new OnCompletionListener() {
			
			@Override
			public void onCompletion(MediaPlayer mp) {
				// TODO Auto-generated method stub
				Toast.makeText(ShowVideoActivity.this, "*** The End ***", Toast.LENGTH_SHORT);
				ShowVideoActivity.this.finish();
				return;
			}
		});
*/	
	}
	
    // When you change direction of phone, this method will be called.
    // It store the state of video (Current position)
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
 
        // Store current position. 
        savedInstanceState.putInt("CurrentPosition", videoview.getCurrentPosition());
//        videoview.pause();
    }
 
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }
    
    // After rotating the phone. This method is called.
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
 
        // Get saved position.
        position = savedInstanceState.getInt("CurrentPosition");
        videoview.seekTo(position);
    }
    
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		 super.onBackPressed();
		return;
	}
}
