package net.qsoft.brac.bmfpo;

import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import net.qsoft.brac.bmfpo.data.DAO;

import java.util.ArrayList;
import java.util.HashMap;

public class CListActivity extends SSActivity {
    private static final String TAG = CListActivity.class.getSimpleName();

    ListView lv = null;
    ArrayList<HashMap<String, String>> items = null;
    Long vono;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clist);

        lv = (ListView) findViewById(R.id.lstView);

        if (getIntent().hasExtra(P8.VONO)) {
            vono = getIntent().getExtras().getLong(P8.VONO);
            Log.d(TAG, "In has extra " + vono);
            setTitle("Collec. Details VO No.: " + vono);
        } else {
            setTitle("Collection Details");
            if (VERSION.SDK_INT >= VERSION_CODES.HONEYCOMB) {
                getActionBar().setDisplayHomeAsUpEnabled(true);
            }
        }

        createCollectionList(vono);

    }

    private void createCollectionList(Long von) {
        Log.d(TAG, "VO No.: " + (von == null ? "" : von));
        DAO da = new DAO(this);
        da.open();
        items = da.getCollectionItems(von);
        da.close();

        CListAdapter adapter = new CListAdapter(this, R.layout.cl_gp_detail, items);
        lv.setAdapter(adapter);
    }


    public void onCancel(View view) {
        // Back
        finish();
    }

}
