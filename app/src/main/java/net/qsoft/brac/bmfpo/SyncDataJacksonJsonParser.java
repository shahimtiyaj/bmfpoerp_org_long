package net.qsoft.brac.bmfpo;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.SQLException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MappingJsonFactory;

import net.qsoft.brac.bmfpo.Volley.Utils;
import net.qsoft.brac.bmfpo.Volley.VolleyCustomRequest;
import net.qsoft.brac.bmfpo.data.CLoan;
import net.qsoft.brac.bmfpo.data.CMember;
import net.qsoft.brac.bmfpo.data.DAO;
import net.qsoft.brac.bmfpo.data.PO;
import net.qsoft.brac.bmfpo.data.VO;
import net.qsoft.brac.bmfpo.util.CloudRequest;
import net.qsoft.brac.bmfpo.util.UpdateApk;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import static android.os.Environment.DIRECTORY_DOWNLOADS;
import static com.android.volley.VolleyLog.d;
import static com.android.volley.VolleyLog.v;
import static net.qsoft.brac.bmfpo.MainActivity_new.VolleyErrorResponse;
import static net.qsoft.brac.bmfpo.P8.getDateTime;
import static net.qsoft.brac.bmfpo.data.DBHelper.TBL_CLOANS;
import static net.qsoft.brac.bmfpo.data.DBHelper.TBL_CMEMBERS;
import static net.qsoft.brac.bmfpo.data.DBHelper.TBL_GOOD_LOANS;
import static net.qsoft.brac.bmfpo.data.DBHelper.TBL_TRANS_TRAIL;
import static net.qsoft.brac.bmfpo.data.DBHelper.TBL_TRX_MAPPING;
import static net.qsoft.brac.bmfpo.data.DBHelper.TBL_VOLIST;

public class SyncDataJacksonJsonParser extends Activity {
    private static final String TAG = SyncDataJacksonJsonParser.class.getSimpleName();
    private Handler handler;
    private String req = null;
    private Context context;
    private DownloadManager downloadManager;
    private long refid;
    private long referenceId;
    private ArrayList<Long> list = new ArrayList<>();
    private HashMap<String, String> po;
    private HashMap<Integer, String> loantrxItems;
    private HashMap<Integer, String> saveTrxItems;
    private ArrayList<CMember> cMemList = new ArrayList<CMember>();
    private ArrayList<CLoan> cLoans = new ArrayList<CLoan>();


    private String CoNo = "", BranchCode = "", syncTime, vono, ServerColcFor;
    private int ProjectCode = 0;
    private ProgressDialog progressDialog;
    private Button retry;
    private TextView net, message;
    //private Date ColcDate;
    private String ColcDate;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sync_data_layout);
        initializeViews();
    }

    //JSON Post Request--------------------------------------------
    public void GetDataFromServer() {
        final DAO da = new DAO(getApplicationContext());
        da.open();
        syncTime = getDateTime();
        Log.d("SyncTime:", "SyncTime: " + syncTime);

        String hitURL = App.getSyncUrl();
        HashMap<String, String> params = new HashMap<>();
        try {
            req = da.PrepareDataForSending().toString();
            params.put("req", req);

        } catch (IOException e) {
            e.printStackTrace();
        }

        VolleyCustomRequest postRequest = new VolleyCustomRequest(Request.Method.POST, hitURL, null,
                new Response.Listener<org.json.JSONObject>() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onResponse(org.json.JSONObject response) {
                        Log.d(TAG, "Server Response: " + response.toString());
                        DAO da = new DAO(getApplicationContext());
                        da.open();

                        try {
                            v("Response:%n %s", response.toString(4));
                            String status = response.getString("status");
                            Log.d("Message", status);

                            switch (status) {
                                case "A":
                                    try {
                                        JSONArray data1 = response.getJSONArray("data");
                                        org.json.JSONObject c = data1.getJSONObject(0);
                                        PO po = da.getPO();
                                        if (po.isPO()) {
                                            POExistsALert(c.getString("cono"), c.getString("coname"),
                                                    c.getString("branchcode"), c.getString("branchname"),
                                                    c.getString("opendate"), " ",
                                                    c.getString("projectcode"), c.getInt("SLno"));

                                        } else {
                                            da.AssignCollector(c.getString("cono"), c.getString("coname"),
                                                    c.getString("branchcode"), c.getString("branchname"),
                                                    c.getString("opendate"), " ",
                                                    c.getString("projectcode"), c.getInt("SLno"));
                                            Intent intent = new Intent(getApplicationContext(), MainActivity_new.class);
                                            startActivity(intent);
                                        }

                                    } catch (Exception e) {
                                        Log.d(TAG, e.getMessage());
                                    }
                                    break;
                                case "APK":
                                    progressDialog.dismiss();
                                    String apkUrl = response.getString("apk_url");
                                    Log.d(TAG, apkUrl);
                                    UpdateApk updateApk = new UpdateApk(SyncDataJacksonJsonParser.this);
                                    updateApk.startDownloadingApk(apkUrl);

                                case "PEND":
                                    AlertDialog(R.drawable.ic_open_in_browser_black_24dp, "Pending Data !", "Still data is pending in sync cloud.Please try later.");
                                    break;
                                case "CT": //CT==current time
                                    AlertDialog(R.drawable.ic_open_in_browser_black_24dp, "Time Mismatch!", "Please Update your Device time.");
                                    break;
                                case "HP":
                                    AlertDialog(R.drawable.ic_open_in_browser_black_24dp, "Pending Server Data !", "Still data is pending in server.Please try later.");
                                    break;
                                case "VEN": //VEN==Vendor
                                    AlertDialog(R.drawable.ic_open_in_browser_black_24dp, "Store Server error ", "Unable to process request. Please contact with your vendor ");
                                    break;
                                case "UL":
                                    try {
                                        da.ClearTransactions();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                case "D":
                                    DownloadJsonFile();
                                    break;

                                case "INA": //INA = In Active Device
                                    AlertDialog(R.drawable.ic_perm_device_information_black_24dp, "Deactivate Device !", "Your Device is Deactivate.Please Active your device first then try.");
                                    progressDialog.dismiss();
                                    break;
                                case "ASG": //ASG = Already Assign Device
                                    String message = response.getString("message");
                                    AlertDialog(R.drawable.ic_perm_device_information_black_24dp, "Already Assigned!", message);
                                    progressDialog.dismiss();
                                    break;
                                case "CL"://CL = Branch Closed
                                    AlertDialog(R.drawable.ic_do_not_disturb_off_black_24dp, "Branch Closed !", "The Branch you have requested to get data is closed at this moment, Please try again later.Thank you.");
                                    progressDialog.dismiss();
                                    break;
                                case "BCL"://CL = Branch not found
                                    AlertDialog(R.drawable.ic_do_not_disturb_off_black_24dp, "Branch Not Found!", "The Branch you have requested to get data is not found, Please try again later.Thank you.");
                                    progressDialog.dismiss();
                                    break;
                                case "F"://F = Device not found
                                    AlertDialog(R.drawable.ic_do_not_disturb_off_black_24dp, "Device Not Found!", "This Device is not found !!");
                                    progressDialog.dismiss();
                                    break;
                                default:
                                    AlertDialog(R.drawable.ic_perm_device_information_black_24dp, "Device Not Assign !", "Your Device is not Assign yet.Please Assign your device first then try.Thank You !");
                                    progressDialog.dismiss();
                                    break;
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                        } finally {
                            da.close();
                        }
                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        progressDialog.dismiss();
                        volleyError.printStackTrace();
                        d(TAG, "Error: " + volleyError.getMessage());
                        P8.ShowError(SyncDataJacksonJsonParser.this, VolleyErrorResponse(volleyError));
                    }
                }) {

            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                int AppVersionCode = BuildConfig.VERSION_CODE;
                String AppVersionName = BuildConfig.VERSION_NAME;
                String AppId = BuildConfig.APPLICATION_ID.substring(15);

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("ProjectCode", String.valueOf(ProjectCode));
                headers.put("PIN", CoNo);
                headers.put("BranchCode", BranchCode);
                headers.put("Content-Type", "application/json");
                headers.put("ApiKey", "7f30f4491cb4435984616d1913e88389");
                headers.put("ImeiNo", App.getDeviceId());
                headers.put("LastSyncTime", da.GetSyncTime());
                headers.put("CurrentTime", CurrentDateTime());
                headers.put("AppVersionCode", String.valueOf(AppVersionCode));
                headers.put("AppVersionName", AppVersionName);
                headers.put("AppId", AppId);
                headers.put("req", req);
                return headers;
            }
        };

        int socketTimeout = 1800000;//18 Minutes  - change to what you want// 18,0,0,000 miliseconds=18 minutes
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        CloudRequest.getInstance(this).addToRequestQueue(postRequest);
    }

    //JSON Post Request--------------------------------------------
    public void SendLastDownloadStatus() {
        String hitURL = App.getLastDownloadStatusURL();
        VolleyCustomRequest postRequest = new VolleyCustomRequest(Request.Method.POST, hitURL, null,
                new Response.Listener<org.json.JSONObject>() {
                    @Override
                    public void onResponse(org.json.JSONObject response) {
                        Log.d("response", "Size: " + response.length());
                        try {
                            v("Response:%n %s", response.toString(4));
                            String status = response.getString("message");
                            Log.d("Message", status);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        d(TAG, "Error: " + volleyError.getMessage());
                        P8.ShowError(SyncDataJacksonJsonParser.this, VolleyErrorResponse(volleyError));
                    }
                }) {

            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                final int AppVersionCode = BuildConfig.VERSION_CODE;
                final String AppVersionName = BuildConfig.VERSION_NAME;
                final String AppId = BuildConfig.APPLICATION_ID.substring(15);
                final String DownloadStatus = "completed";

                DAO dao = new DAO(getApplicationContext());
                dao.open();
                dao.close();

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("ProjectCode", String.valueOf(ProjectCode));
                headers.put("BranchCode", BranchCode);
                headers.put("ApiKey", "7f30f4491cb4435984616d1913e88389");
                headers.put("ImeiNo", App.getDeviceId());
                headers.put("PIN", CoNo);
                headers.put("LastSyncTime", dao.GetSyncTime());
                headers.put("LastDownloadStatus", DownloadStatus);
                headers.put("CurrentTime", CurrentDateTime());
                headers.put("AppVersionCode", String.valueOf(AppVersionCode));
                headers.put("AppVersionName", AppVersionName);
                headers.put("AppId", AppId);

                return headers;
            }
        };
        // Add the request to the RequestQueue.
        CloudRequest.getInstance(this).addToRequestQueue(postRequest);
    }

    private class DownLoadAsyncTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected Void doInBackground(Void... params) {

            final Thread thread = new Thread() {
                @Override
                public void run() {
                    Log.d("Load ServerData Thread ", "Started..");
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                progressDialog = new ProgressDialog(SyncDataJacksonJsonParser.this);
                                progressDialog.setCancelable(false);
                                progressDialog.setTitle(" Loading Data");
                                progressDialog.setMessage("Please wait...");
                                progressDialog.show();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    });
                    GetDataFromServer();
                }
            };
            thread.start();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (progressDialog.isShowing()) progressDialog.dismiss();
            progressDialog = null;
        }
    }

    private class ReadJSONAsyncTask extends AsyncTask<Void, Void, Void> {
        private final ReentrantLock lock = new ReentrantLock();
        private final Condition tryAgain = lock.newCondition();
        private volatile boolean finished = false;
        private Context context;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                progressDialog = new ProgressDialog(SyncDataJacksonJsonParser.this);
                progressDialog.setCancelable(false);
                progressDialog.setTitle(" Updating Data");
                progressDialog.setMessage("Please wait...");
                progressDialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected Void doInBackground(Void... params) {

            Log.d("ReadJSONFile:  ", "Started..");

            try {
                VoMemCloanGoodLoanRead();
                //receiveAmountInserted();
            } catch (IOException e) {
                e.printStackTrace();
            }

//            final Thread thread = new Thread() {
//                @Override
//                public void run() {
//                    Log.d("Json Read Thread ", "Started..");
//                    handler.post(new Runnable() {
//                        @Override
//                        public void run() {
//                            progressDialog = new ProgressDialog(SyncDataJacksonJsonParser.this);
//                            progressDialog.setCancelable(false);
//                            progressDialog.setTitle(" Updating Data");
//                            progressDialog.setMessage("Please wait...");
//                            progressDialog.show();
//                        }
//                    });
//                    try {
//                        VoMemCloanGoodLoanRead();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//            };
//            thread.start();
            return null;
        }

        public void runAgain() {
            // Call this to request data from the server again
            tryAgain.signal();
        }

        public void terminateTask() {
            // The task will only finish when we call this method
            finished = true;
            lock.unlock();
        }

        @Override
        protected void onCancelled() {
            // Make sure we clean up if the task is killed
            terminateTask();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

    public void VoMemCloanGoodLoanRead() throws IOException {
        DAO da = new DAO(getApplicationContext());
        da.open();
        PO po = da.getPO();
        String cono = po.get_CONo();

        unpackZip("/storage/emulated/0/Download/", cono + "-bmfpoerp.zip");

        JsonFactory f = new MappingJsonFactory();
        JsonParser jp = f.createParser(new File("/storage/emulated/0/Download/" + cono + "results.json"));

        JsonToken current;

        current = jp.nextToken();
        if (current != JsonToken.START_OBJECT) {
            Log.d("Error", "root should be object: quiting.");
            return;
        }

        while (jp.nextToken() != JsonToken.END_OBJECT) {

            String fieldName = jp.getCurrentName();

            current = jp.nextToken();

            if (fieldName.equals("data")) {

                if (current == JsonToken.START_OBJECT) {

                    while (jp.nextToken() != JsonToken.END_OBJECT) {

                        String fieldName1 = jp.getCurrentName();

                        current = jp.nextToken();

                        switch (fieldName1) {
                            case "settings":

                                if (current == JsonToken.START_ARRAY) {

                                    while (jp.nextToken() != JsonToken.END_ARRAY) {

                                        JsonNode node = jp.readValueAsTree();
                                        try {
                                            Log.d("Inserting ", "Setting table");
                                            // da.UpdateSMSstatus(String.valueOf(node.get("SMS").asText()));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                        Log.d("Inserting Settings", String.valueOf(node));
                                    }
                                }
                                break;

                            case "volist":

                                if (current == JsonToken.START_ARRAY) {

                                    while (jp.nextToken() != JsonToken.END_ARRAY) {

                                        JsonNode node = jp.readValueAsTree();

                                        try {

                                            DAO.executeSQL("INSERT OR REPLACE INTO " + TBL_VOLIST + "(OrgNo, OrgName, OrgCategory, MemSexID, " +
                                                            "SavColcOption, LoanColcOption, SavColcDate, LoanColcDate, CONo, TargetDate, StartDate, " +
                                                            "EndDate, NextTargetDate, Territory, UpdatedAt, ProjectCode, BranchCode) " +
                                                            "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",

                                                    new String[]{
                                                            String.valueOf(node.get("OrgNo").asText()),
                                                            String.valueOf(node.get("OrgName").asText()),
                                                            " ",// c.getString("OrgCategory"),
                                                            String.valueOf(node.get("MemSexId").asText()),
                                                            String.valueOf(node.get("ColcOption").asText()),//SavColcOption
                                                            String.valueOf(node.get("ColcOption").asText()),//LoanColcOption
                                                            String.valueOf(node.get("TargetDate").asText()),//SavColcDate
                                                            String.valueOf(node.get("TargetDate").asText()),//LoanColcDate
                                                            String.valueOf(node.get("PIN").asText()),
                                                            String.valueOf(node.get("TargetDate").asText()),
                                                            String.valueOf(node.get("StartDate").asText()),
                                                            " ",//c.getString("EndDate"),
                                                            String.valueOf(node.get("NextTargetDate").asText()),
                                                            " ",//c.getString("Territory"),
                                                            String.valueOf(node.get("UpdatedAt").asText()),
                                                            String.valueOf(node.get("ProjectCode").asText()),
                                                            BranchCode
                                                    });
                                            Log.d("Inserting VOList", String.valueOf(node));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }

                                Log.d("VOList", "Inserted Successfully !");

                                break;
                            case "cmembers":

                                if (current == JsonToken.START_ARRAY) {

                                    while (jp.nextToken() != JsonToken.END_ARRAY) {

                                        JsonNode node = jp.readValueAsTree();
                                        try {

                                            String ColcDate = da.GetTargetDate(String.valueOf(node.get("OrgNo").asInt()));
                                            //String receAmt = da.ReceAmtCalculate(String.valueOf(node.get("OrgNo").asInt()));

                                            DAO.executeSQL("INSERT OR REPLACE INTO " + TBL_CMEMBERS + "(ProjectCode, OrgNo, OrgMemNo, " +
                                                            "MemberName, SavBalan, SavPayable, CalcIntrAmt, TargetAmtSav, ReceAmt, " +
                                                            "ColcDate, AdjAmt, SB, SIA, SavingsRealized, " +
                                                            "MemSexID, NationalIDNo, PhoneNo, Walletno, AdmissionDate) " +
                                                            "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",

                                                    new String[]{
                                                            String.valueOf(node.get("ProjectCode").asText()),
                                                            String.valueOf(node.get("OrgNo").asText()),
                                                            String.valueOf(node.get("OrgMemNo").asText()),
                                                            String.valueOf(node.get("MemberName").asText()),
                                                            String.valueOf(node.get("SavBalan").asText()),
                                                            String.valueOf(node.get("SavPayable").asText()),
                                                            String.valueOf(node.get("CalcIntrAmt").asText()),
                                                            String.valueOf(node.get("TargetAmtSav").asText()),
                                                            "0",//String.valueOf(node.get("ReceAmt").asText()),
                                                            ColcDate,
                                                            "null", //c.getString("AdjAmt"),
                                                            String.valueOf(node.get("SavBalan").asText()),//SB
                                                            String.valueOf(node.get("TargetAmtSav").asText()),//SIA
                                                            "null", //c.getString("SavingsRealized"),
                                                            String.valueOf(node.get("MemSexId").asText()),
                                                            String.valueOf(node.get("NationalId").asText()),
                                                            String.valueOf(node.get("ContactNo").asText()),
                                                            String.valueOf(node.get("BkashWalletNo").asText()),
                                                            String.valueOf(node.get("ApplicationDate").asText())

                                                    });

                                            Log.d("Inserting CMembers", String.valueOf(node));

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }

                                Log.d("CMembers", "Inserted Successfully !");

                                break;
                            case "cloans":

                                if (current == JsonToken.START_ARRAY) {

                                    while (jp.nextToken() != JsonToken.END_ARRAY) {

                                        JsonNode node = jp.readValueAsTree();

                                        try {
                                            int LnSts = Integer.parseInt(String.valueOf(node.get("LnStatus").asInt()));
                                            int LnStatus = LnSts - 1;

                                            String ColcDate = da.GetTargetDate(String.valueOf(node.get("OrgNo").asInt()));


                                            DAO.executeSQL("INSERT OR REPLACE INTO " + TBL_CLOANS + "(OrgNo, OrgMemNo, ProjectCode, LoanNo, LoanSlNo, " +
                                                            "ProductNo, IntrFactorLoan, PrincipalAmt, SchmCode, InstlAmtLoan, DisbDate, LnStatus, " +
                                                            "PrincipalDue, InterestDue, TotalDue, TargetAmtLoan, TotalReld, Overdue, BufferIntrAmt, " +
                                                            "ReceAmt, IAB, ODB, TRB, LB, PDB, IDB, " +
                                                            "InstlPassed, OldInterestDue, ProductSymbol, LoanRealized, ColcDate) " +
                                                            "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " +
                                                            "?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                                                    new String[]{
                                                            String.valueOf(node.get("OrgNo").asText()),
                                                            String.valueOf(node.get("OrgMemNo").asText()),
                                                            String.valueOf(node.get("ProjectCode").asText()),
                                                            String.valueOf(node.get("LoanNo").asText()),
                                                            String.valueOf(node.get("LoanSlNo").asText()),
                                                            String.valueOf(node.get("ProductNo").asText()),
                                                            String.valueOf(node.get("IntrFactorLoan").asText()),
                                                            String.valueOf(node.get("PrincipalAmount").asText()),
                                                            "null", //c.getString("SchmCode"),
                                                            String.valueOf(node.get("InstlAmtLoan").asText()),
                                                            String.valueOf(node.get("DisbDate").asText()),
                                                            String.valueOf(LnStatus),
                                                            String.valueOf(node.get("PrincipalDue").asText()),
                                                            String.valueOf(node.get("InterestDue").asText()),
                                                            String.valueOf(node.get("TotalDue").asText()),
                                                            String.valueOf(node.get("TargetAmtLoan").asText()),
                                                            String.valueOf(node.get("TotalReld").asText()),
                                                            String.valueOf(node.get("Overdue").asText()),
                                                            String.valueOf(node.get("BufferIntrAmt").asText()),
                                                            "null",//c.getString("ReceAmt"),
                                                            String.valueOf(node.get("TargetAmtLoan").asText()),//IAB
                                                            String.valueOf(node.get("Overdue").asText()),//ODB
                                                            String.valueOf(node.get("TotalReld").asText()),//TRB
                                                            String.valueOf(node.get("TotalDue").asText()),//LB
                                                            String.valueOf(node.get("PrincipalDue").asText()),//PDB
                                                            String.valueOf(node.get("InterestDue").asText()),//IDB
                                                            String.valueOf(node.get("InstlPassed").asText()),
                                                            "0", //c.getString("OldInterestDue"),
                                                            String.valueOf(node.get("ProductSymbol").asText()),
                                                            "null",// c.getString("LoanRealized"),
                                                            ColcDate
                                                    });

                                            Log.d("Inserting Cloans", String.valueOf(node));

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                                Log.d("Cloans", "Inserted Successfully !");

                                if (progressDialog.isShowing()) progressDialog.dismiss();
                                progressDialog = null;
                                da.WriteSyncTime(syncTime);
                                da.UpdateEMethod(1);
                                Intent intent = new Intent(SyncDataJacksonJsonParser.this, MainActivity_new.class);
                                startActivity(intent);
                                Log.d("Open Main Activity", CurrentDateTime());

                                break;
                            case "goodloans":

                                if (current == JsonToken.START_ARRAY) {

                                    while (jp.nextToken() != JsonToken.END_ARRAY) {

                                        JsonNode node = jp.readValueAsTree();
                                        try {

                                            DAO.executeSQL("INSERT OR REPLACE INTO " + TBL_GOOD_LOANS + "(OrgNo, OrgMemNo, MaxAmt," +
                                                            " Taken, UpdatedAt, _Month, _Year) " +
                                                            "VALUES(?, ?, ?, ?, ?, ?, ?)",

                                                    new String[]{
                                                            String.valueOf(node.get("OrgNo").asText()),
                                                            String.valueOf(node.get("OrgMemNo").asText()),
                                                            String.valueOf(node.get("MaxAmt").asText()),
                                                            String.valueOf(node.get("Taken").asText()),
                                                            String.valueOf(node.get("UpdatedAt").asText()),
                                                            String.valueOf(node.get("Month").asText()),
                                                            String.valueOf(node.get("Year").asText())
                                                    });

                                            Log.d("Inserting GoodLoans", String.valueOf(node));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                                Log.d("GoodLoans", "Inserted Successfully !");

                                break;

                            case "transtrail":

                                if (current == JsonToken.START_ARRAY) {

                                    while (jp.nextToken() != JsonToken.END_ARRAY) {

                                        JsonNode node = jp.readValueAsTree();

                                        String Transno = null;
                                        String loanTrx;
                                        String saveTrx;
                                        String transno;
                                        int trx = 0;

                                        if (node.get("TrxType").asInt() > 0) {

                                            String ColcDate = da.GetTargetDate(String.valueOf(node.get("OrgNo").asInt()));
                                            String ServerColcFor = node.get("ColcFor").asText();
                                            transno = P8.LeftPad(String.valueOf(node.get("TransNo").asInt()), 11);

                                            try {
                                                trx = Integer.parseInt(String.valueOf((node.get("TrxType")).asInt()));
                                            } catch (NumberFormatException num) {
                                                num.printStackTrace();
                                                continue;
                                            }

                                            // Log.d("TrxType ", "Position:" + node + " :" + trx);

                                            for (int x = 1; x <= loantrxItems.size(); x++) {
                                                if (trx == x && ServerColcFor.equals("L")) {
                                                    loanTrx = loantrxItems.get(x).concat(transno);
                                                    Transno = loanTrx;
                                                } else {
                                                    for (int y = 1; y <= saveTrxItems.size(); y++) {
                                                        if (trx == y && ServerColcFor.equals("S")) {
                                                            saveTrx = saveTrxItems.get(y).concat(transno);
                                                            Transno = saveTrx;
                                                        }
                                                    }
                                                }
                                            }

                                            DAO.executeSQL("INSERT OR REPLACE INTO " + TBL_TRANS_TRAIL + "(OrgNo, OrgMemNo, Projectcode, " +
                                                            "LoanNo, Tranamount, Colcdate, Transno, TrxType, ColcFor, UpdatedAt) " +
                                                            "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",

                                                    new String[]{
                                                            String.valueOf(node.get("OrgNo").asInt()),
                                                            String.valueOf(node.get("OrgMemNo").asInt()),
                                                            String.valueOf(node.get("ProjectCode").asInt()),
                                                            String.valueOf(node.get("LoanNo")),
                                                            String.valueOf(node.get("Tranamount")),
                                                            String.valueOf(node.get("ColcDate").asText()),
                                                            Transno,
                                                            String.valueOf(node.get("TrxType").asText()),
                                                            String.valueOf(node.get("ColcFor").asText()),
                                                            String.valueOf(node.get("UpdatedAt").asText())
                                                    });

                                            Log.d("Inserting TransTrail", String.valueOf(node));
                                        }
                                    }
                                }

                                Log.d("TransTrail ", "Updated Successfully !!");

                                break;
                            //End TransTrail
                        }

                    }

                } else {
                    Log.d("Error", "records should be an array: skipping.");
                    jp.skipChildren();
                }
            } else {
                Log.d("Unprocessed property", fieldName);
                jp.skipChildren();
            }
        }
        Log.d("Inserted Finished ", CurrentDateTime());
        DeletePOjsonFile();
        DeletePOzipFile();
        da.close();
    }

    public void DeletePOjsonFile() {
        Uri uriPOJson = Uri.parse("/storage/emulated/0/Download/" + CoNo + "results.json");
        File jsonPOfdelete = new File(uriPOJson.getPath());
        if (jsonPOfdelete.exists()) {
            if (jsonPOfdelete.delete()) {
                Log.d("BMFPO Json File:", "Deleted !");
            } else {
                Log.d("BMFPO Json File:", "Not Found !");
            }
        }
    }

    public void DeletePOzipFile() {
        Uri uriPOZip = Uri.parse("/storage/emulated/0/Download/" + CoNo + "-bmfpoerp.zip");

        File zipPOfdelete = new File(uriPOZip.getPath());

        if (zipPOfdelete.exists()) {
            if (zipPOfdelete.delete()) {
                Log.d("BMFPO Zip File:", "Deleted !");
                // Toast.makeText(getApplicationContext(), "Previous File Deleted" + uriZip.getPath(), Toast.LENGTH_LONG).show();
            } else {
                // Toast.makeText(getApplicationContext(), "Previous File not Deleted" + uriZip.getPath(), Toast.LENGTH_LONG).show();
                Log.d("BMFPO Zip File:", "Not Found !");
            }
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void initializeViews() {
        handler = new Handler();
        setTitle("Sync Data");
        Runtime rt = Runtime.getRuntime();
        long maxMemory = rt.maxMemory();
        Log.v("onCreate", "maxMemory:" + Long.toString(maxMemory));
        //Call Downloading function----------------------
        DownloadManagerInitialize();
        // TrxType data loading
        TrxTypedata();
        //init progress dilog
        progressDialog = new ProgressDialog(SyncDataJacksonJsonParser.this);
        retry = (Button) findViewById(R.id.retry);
        retry.setVisibility(View.INVISIBLE);
        net = (TextView) findViewById(R.id.net);
        net.setVisibility(View.INVISIBLE);
        message = (TextView) findViewById(R.id.message);
        message.setVisibility(View.INVISIBLE);


        //Database instance------------------------------
        DAO da = new DAO(getApplicationContext());
        da.open();
        PO po = da.getPO();
        CoNo = po.get_CONo();
        BranchCode = po.get_BranchCode();
        ProjectCode = po.get_ProjectCode();
        da.close();
        if (Utils.isNetworkAvailable(App.getContext())) {
            new DownLoadAsyncTask().execute();
        } else {
            retry.setVisibility(View.VISIBLE);
            retry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    createNetErrorDialog();
                }
            });
            net.setVisibility(View.VISIBLE);
            message.setVisibility(View.VISIBLE);
        }
    }

    protected void createNetErrorDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("You need internet connection for this app. Please turn on mobile network or Wi-Fi in Settings.")
                .setTitle("Unable to connect internet")
                .setIcon(R.drawable.wifi)
                .setCancelable(false)
                .setPositiveButton("Settings",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent i = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                                startActivity(i);
                            }
                        }
                )
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                SyncDataJacksonJsonParser.this.finish();
                            }
                        }
                );
        AlertDialog alert = builder.create();
        alert.show();
    }

    public String CurrentDateTime() {
        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dt.format(date);
    }

    public void DownloadManagerInitialize() {
        downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }

    public void DownloadJsonFile() {
        list.clear();
        Uri uri = Uri.parse(App.getDownloadDataURL() + CoNo + ".zip");
        DownloadManager.Request request = new DownloadManager.Request(uri);
        if (DownloadManager.ERROR_HTTP_DATA_ERROR == 404) {
            Toast.makeText(SyncDataJacksonJsonParser.this, "Error", Toast.LENGTH_LONG).show();
        }
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
        request.setAllowedOverRoaming(false);
        request.setTitle("BMFPO-ERP" + CoNo + "-bmfpoerp.zip");
        request.setDescription("BMFPO Data Downloading" + CoNo + "-bmfpoerp.zip");
        request.setVisibleInDownloadsUi(true);
        request.setDestinationInExternalPublicDir(DIRECTORY_DOWNLOADS, CoNo + "-bmfpoerp.zip");
        request.allowScanningByMediaScanner();
        refid = downloadManager.enqueue(request);
        Log.e("OUTNM", "" + refid);
        list.add(refid);
    }

    BroadcastReceiver onComplete = new BroadcastReceiver() {

        public void onReceive(Context ctxt, Intent intent) {

            long referenceId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);

            String action = intent.getAction();

            if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {

                DownloadManager.Query query = new DownloadManager.Query();

                query.setFilterById(referenceId);

                Cursor c = downloadManager.query(query);

                if (c.moveToFirst()) {
                    try {


                        int columnIndex = c.getColumnIndex(DownloadManager.COLUMN_STATUS);

                        if (DownloadManager.STATUS_SUCCESSFUL == c.getInt(columnIndex)) {
                            Log.e("IN", "" + referenceId);
                            list.remove(referenceId);

                            if (list.isEmpty()) {
                                Log.e("INSIDE", "" + referenceId);
                                NotificationCompat.Builder mBuilder =
                                        new NotificationCompat.Builder(SyncDataJacksonJsonParser.this, "channelId")
                                                .setSmallIcon(R.drawable.ic_launcher)
                                                .setContentTitle("BMFPO Data").setContentText("PO Data Download completed!");
                                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                                notificationManager.notify(455, mBuilder.build());

                                SendLastDownloadStatus();
                                progressDialog.dismiss();
                                new ReadJSONAsyncTask().execute();
                            }

                        } else if (DownloadManager.STATUS_FAILED == c.getInt(columnIndex)) {

                            Log.i("handleData()", "Reason: " + c.getInt(c.getColumnIndex(DownloadManager.COLUMN_REASON)));

                            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(SyncDataJacksonJsonParser.this, "channelId")
                                    .setSmallIcon(R.drawable.ic_launcher).setContentTitle("PO Data Download Failed!!");
                            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                            notificationManager.notify(455, mBuilder.build());
                            progressDialog.dismiss();
                            AlertDialog(R.drawable.ic_error_black_24dp, "Download Failed !!", "Please , Try to download the file later.");

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }

            registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
        }
    };

    private boolean unpackZip(String path, String zipname) {
        InputStream is;
        ZipInputStream zis;
        try {
            is = new FileInputStream(path + zipname);
            zis = new ZipInputStream(new BufferedInputStream(is));
            ZipEntry ze;

            while ((ze = zis.getNextEntry()) != null) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                byte[] buffer = new byte[1024];
                int count;

                String filename = ze.getName();
                FileOutputStream fout = new FileOutputStream(path + filename);

                // reading and writing
                while ((count = zis.read(buffer)) != -1) {
                    baos.write(buffer, 0, count);
                    byte[] bytes = baos.toByteArray();
                    fout.write(bytes);
                    baos.reset();
                }

                fout.close();
                zis.closeEntry();
            }

            zis.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }


    public void AlertDialog(int drawable, String title, String message) {
        new AlertDialog.Builder(this)
                .setIcon(drawable)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(getApplicationContext(), MainActivity_new.class);
                        startActivity(intent);
                    }
                }).show();
    }

    public void POExistsALert(final String cono, final String coName, final String brachCode,
                              final String branchName, final String branchOpenDate, final String SysDate, final String projectCode, final int sl) {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Attention !")
                .setMessage("Are you sure you want to assign as new PO? If you want to assign this PO your previous data will be lost!")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        DAO da = new DAO(App.getContext());
                        da.open();
                        try {
                            da.AssignCollector(cono, coName, brachCode, branchName, branchOpenDate, SysDate, projectCode, sl);
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            da.close();
                        }

                        Toast.makeText(getApplicationContext(), "PO Data Updated!", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(getApplicationContext(), MainActivity_new.class);
                        startActivity(intent);
                    }

                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Toast.makeText(getApplicationContext(), "POST Transact Data!", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(getApplicationContext(), MainActivity_new.class);
                        startActivity(intent);
                    }
                }).show();
    }

    public void TrxTypedata() {
        loantrxItems = new HashMap<Integer, String>();
        saveTrxItems = new HashMap<Integer, String>();
        //Saving TrxType Used
        saveTrxItems.put(1, "SC");
        saveTrxItems.put(2, "SD");
        saveTrxItems.put(3, "ST");
        saveTrxItems.put(4, "YI");
        saveTrxItems.put(6, "SD");
        saveTrxItems.put(7, "EC");
        saveTrxItems.put(8, "SC");
        saveTrxItems.put(9, "ST");
        //Saving TrxType probable used
        saveTrxItems.put(5, "IC");//Info change
        saveTrxItems.put(10, "TT");//Transferred
        // loan TrxType Used
        loantrxItems.put(3, "LC");
        loantrxItems.put(7, "ST");
        loantrxItems.put(13, "LC");
        //Loan TrxType  Not Allowed yet
        loantrxItems.put(1, "LD");
        loantrxItems.put(2, "LP");
        loantrxItems.put(4, "LR");
        loantrxItems.put(5, "RS");
        loantrxItems.put(6, "LS");
        loantrxItems.put(8, "GA");
        //Loan TrxType Probable use for future
        loantrxItems.put(9, "WP");
        loantrxItems.put(10, "WD");
        loantrxItems.put(11, "CS");
        loantrxItems.put(12, "NS");
        loantrxItems.put(14, "WI");
        loantrxItems.put(15, "WF");
        loantrxItems.put(16, "TT");

        for (int i = 1; i <= loantrxItems.size(); i++) {
            String loanTrx = loantrxItems.get(i);
            DAO.executeSQL("INSERT OR REPLACE INTO " + TBL_TRX_MAPPING + "(TrxShortType, TrxTypeValue, ColcFor) " +
                            "VALUES(?, ?, ?)",
                    new String[]{loanTrx, String.valueOf(i), "L"});
        }
        for (int i = 1; i <= saveTrxItems.size(); i++) {
            String saveTrx = saveTrxItems.get(i);
            DAO.executeSQL("INSERT OR REPLACE INTO " + TBL_TRX_MAPPING + "(TrxShortType, TrxTypeValue, ColcFor) " +
                            "VALUES(?, ?, ?)",
                    new String[]{saveTrx, String.valueOf(i), "S"});
        }
    }

    public void receiveAmountInserted() {
        try {
            DAO da = new DAO(getApplicationContext());
            da.open();
            cMemList = da.getAllCMemebers();
            for (int i = 0; i < cMemList.size(); i++) {
                final Long orgNo = cMemList.get(i).get_OrgNo();
                final String receiveAmt = da.ReceAmtCalculate(orgNo.toString());
                Log.d("Position:" + i + ": " + orgNo.toString(), " Receive Amount:" + receiveAmt);
                da.UpdateReceiveAmt(receiveAmt, orgNo.toString());
            }
            da.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
      /*  if (progressDialog != null)
            progressDialog.dismiss(); */
        // finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
        // finish();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    public void onCancel(View view) {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(onComplete);
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

}
