package net.qsoft.brac.bmfpo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import net.qsoft.brac.bmfpo.data.DBHelper;

import java.util.HashMap;

public class WPMemberActivity extends MemberActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	    lv.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            	HashMap<String, String> x = (HashMap<String, String>) lv.getItemAtPosition(position);
            	Long vono = Long.valueOf(x.get(DBHelper.FLD_ORG_NO));
            	Long memno = Long.valueOf(x.get(DBHelper.FLD_ORG_MEM_NO));
            	Intent it = new Intent(getApplicationContext(), WPMempfActivity.class);
            	it.putExtra(P8.VONO, vono);
            	it.putExtra(P8.MEMNO, memno);
            	startActivity(it);
            	
//                Toast.makeText(getBaseContext(), vono + "-" + memno, Toast.LENGTH_LONG).show();
            }
        });
		
	}
	/*
	public void setupList() {
		DAO da = new DAO(this);
		da.open();
		memlist = da.getWPMemberList();
		da.close();
	}
	*/
}
