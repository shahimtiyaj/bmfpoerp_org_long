package net.qsoft.brac.bmfpo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

public class ODRListAdapter extends ArrayAdapter<HashMap<String, String>> {
	// View lookup cache
    private static class ViewHolder {
    	TextView gphead;
        TextView memno;
        TextView memname;
        TextView disbamt;
        TextView disbdate;
        TextView overdue;
    }

	public ODRListAdapter(Context context, int resource, List<HashMap<String, String>> rows) {
		super(context, resource, rows);
		// TODO Auto-generated constructor stub
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
	       // Get the data item for this position
	       HashMap<String, String> row = getItem(position);    
	       // Check if an existing view is being reused, otherwise inflate the view
	       ViewHolder viewHolder; // view lookup cache stored in tag
	       if (convertView == null) {
	          viewHolder = new ViewHolder();
	          LayoutInflater inflater = LayoutInflater.from(getContext());
	          convertView = inflater.inflate(R.layout.ordreport_row, parent, false);
	          viewHolder.gphead = (TextView) convertView.findViewById(R.id.textGPHead);
	          viewHolder.memno = (TextView) convertView.findViewById(R.id.textMemNo);
	          viewHolder.memname = (TextView) convertView.findViewById(R.id.textMemName);
	          viewHolder.disbamt = (TextView) convertView.findViewById(R.id.textDisbAmt);
	          viewHolder.disbdate = (TextView) convertView.findViewById(R.id.textDisbDate);
	          viewHolder.overdue = (TextView) convertView.findViewById(R.id.textOverdue);
	          
	          convertView.setTag(viewHolder);
	       } else {
	           viewHolder = (ViewHolder) convertView.getTag();
	       }
	       
	       // Populate the data into the template view using the data object
	       if(row.get("GPHead").isEmpty()) {
	    	   viewHolder.gphead.setVisibility(View.GONE);
	       }
	       else {
	    	   viewHolder.gphead.setVisibility(View.VISIBLE);
	    	   viewHolder.gphead.setText(row.get("GPHead"));
//	    	   viewHolder.memno.setVisibility(View.GONE);
//	    	   viewHolder.memname.setVisibility(View.GONE);
//	    	   viewHolder.disbamt.setVisibility(View.GONE);
//	    	   viewHolder.overdue.setVisibility(View.GONE);
	       }
	       
	       viewHolder.memno.setText(row.get("MemNo"));
	       viewHolder.memname.setText(row.get("MemberName"));
	       viewHolder.disbamt.setText(row.get("DisbAmt"));
	       viewHolder.disbdate.setText(row.get("DisbDate"));
	       viewHolder.overdue.setText(row.get("Overdue"));
	       
	       // Return the completed view to render on screen
	       return convertView;
	   }	
}
