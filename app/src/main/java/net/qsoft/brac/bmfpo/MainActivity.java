package net.qsoft.brac.bmfpo;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nex3z.notificationbadge.NotificationBadge;

import net.qsoft.brac.bmfpo.data.DAO;
import net.qsoft.brac.bmfpo.data.PO;
import net.qsoft.brac.bmfpo.sls.SeasonalSurveyActivity;
import net.qsoft.brac.bmfpo.util.CloudRequest;
import net.qsoft.brac.bmfpo.util.SMSListener;

import java.net.URLEncoder;
import java.util.HashMap;

//import android.os.StrictMode;

public class MainActivity extends SSActivity implements SMSListener {
    private static final String TAG = MainActivity_new.class.getSimpleName();
    TextView textSyncInfo;
    TextView collLabel;
    TextView branchLabel;
    TextView dlStatusLabel;
    TextView textVersion;
    Button startButton;
    Button repoButton;
    Button slsButton;
    boolean startMenuEnabled = true;
    public PO coll;
    //nex3z notification  badge --
    public NotificationBadge mBadge;
    public int mCount = 10;

    TextView smsCountTxt;
    int pendingSMSCount = 10;
    String CoNo = "";
    int Emethod = 0;
    ProgressDialog progressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
        }
        actionBar.setTitle("BMFPOERP");

        textSyncInfo = (TextView) findViewById(R.id.textSyncInfo);
        collLabel = (TextView) findViewById(R.id.collLabel);
        branchLabel = (TextView) findViewById(R.id.branchLabel);
        dlStatusLabel = (TextView) findViewById(R.id.dlStatuslabel);
        startButton = (Button) findViewById(R.id.startButton);
        repoButton = (Button) findViewById(R.id.repoButton);
        slsButton = (Button) findViewById(R.id.slsButton);
        // textVersion = (TextView) findViewById(R.id.textVersion);
        int versionCode = BuildConfig.VERSION_CODE;
        //String versionName = BuildConfig.VERSION_NAME;
        //textVersion.setText("BMFPO Version " + versionName);

        DAO da = new DAO(getApplicationContext());
        da.open();
        PO po = da.getPO();
        CoNo = po.get_CONo();
        Emethod = po.get_EMethod();
        da.close();

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //invalidateOptionsMenu();
        CheckDataState();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @SuppressLint("NewApi")
    private void CheckDataState() {
        DAO data = new DAO(this);
        data.open();
        coll = data.getPO();

        App.setBranchOPenDate(coll.get_BranchOpenDate());
        App.setLastSyncDate(P8.getLastSyncTime());
        if (coll.get_SMSSupport() > 0)
            App.setSMSActivity(this);
        else
            App.setSMSActivity(null);

        // Seasonal loan support button
        if (coll.get_SurveySL() > 0) {
            slsButton.setVisibility(View.VISIBLE);
        } else {
            slsButton.setVisibility(View.GONE);
        }

        if (coll.isPO()) {
//			Log.d(TAG, "Fro CheckDataState - after if coll.isPO()");

            collLabel.setText(coll.toString());

            //Log.d(TAG, App.getLastSyncGaps());
            textSyncInfo.setText(App.getLastSyncGaps());

            if (coll.get_BranchCode().equals("0000"))
                branchLabel.setText("");
            else
                branchLabel.setText(coll.get_BranchCode() + " - " + coll.get_BranchName());
            branchLabel.setVisibility(View.VISIBLE);
            if (coll.get_EMethod() == PO.EM_DESKTOP) {
                textSyncInfo.setVisibility(View.INVISIBLE);
                dlStatusLabel.setVisibility(View.VISIBLE);
                startButton.setEnabled(false);
                //startMenuEnabled = false;
                repoButton.setEnabled(false);
                slsButton.setEnabled(false);
            } else {
                textSyncInfo.setVisibility(View.VISIBLE);
                dlStatusLabel.setVisibility(View.INVISIBLE);
                startButton.setEnabled(true);
                //startMenuEnabled = true;
                repoButton.setEnabled(true);
                slsButton.setEnabled(true);
                coll.get_OpenDate();
            }
        } else {
//			Log.d(TAG, "Fro CheckDataState - after else coll.isPO()");
            branchLabel.setVisibility(View.INVISIBLE);
            dlStatusLabel.setVisibility(View.INVISIBLE);
            startButton.setEnabled(false);
            // startMenuEnabled = false;
            repoButton.setEnabled(false);
            slsButton.setEnabled(false);
        }
        Log.d(TAG, "From CheckDataState - after if(isPO) block");
        data.close();
        // Make sure we're running on Honeycomb or higher to use ActionBar APIs
/*        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            // Invalidate menu
        	invalidateOptionsMenu();
        } */
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);

        //final MenuItem menuItem = menu.findItem(R.id.action_notifications);

        //  View actionView = MenuItemCompat.getActionView(menuItem);
        //  smsCountTxt = (TextView) actionView.findViewById(R.id.notification_badge);

        // setupBadge();

        return true;
    }


    private void setupBadge() {

        if (smsCountTxt != null) {
            if (pendingSMSCount == 0) {
                if (smsCountTxt.getVisibility() != View.GONE) {
                    smsCountTxt.setVisibility(View.GONE);
                }
            } else {
                smsCountTxt.setText(String.valueOf(Math.min(pendingSMSCount, 99)));
                if (smsCountTxt.getVisibility() != View.VISIBLE) {
                    smsCountTxt.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    /* (non-Javadoc)
     * @see android.app.Activity#onPrepareOptionsMenu(android.view.Menu)
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // TODO Auto-generated method stub
        super.onPrepareOptionsMenu(menu);
        menu.getItem(0).setEnabled(startMenuEnabled);
//		menu.getItem(5).setEnabled(startMenuEnabled);
        return true;
    }

//    @Override
//    public void onBackPressed() {
//        // TODO Auto-generated method stub
//        super.onBackPressed();
//
////		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
////		pm.goToSleep(System.currentTimeMillis() + 0000); // add 10 seconds to let other apps go sleeping
//        return;
//    }

    public void Reports(View v) {
        PopupMenu popup = new PopupMenu(MainActivity.this, v);

        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.reports_menu, popup.getMenu());
        popup.getMenu().getItem(4).setVisible(App.hasSMSSupport()); // .setEnabled(App.hasSMSSupport());

        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.menu_rept_overdue) {
                    startActivityForResult(new Intent(App.getContext(), VOResultActivity.class), R.id.menu_rept_overdue);
                } else if (item.getItemId() == R.id.menu_rept_vo_loan_disb) {
                    startActivityForResult(new Intent(App.getContext(), VOResultActivity.class), R.id.menu_rept_vo_loan_disb);
                } else if (item.getItemId() == R.id.menu_rept_good_loan) {
                    startActivityForResult(new Intent(App.getContext(), VOResultActivity.class), R.id.menu_rept_good_loan);
                }
           	/*     else if (item.getItemId() == R.id.menu_rept_monthly_savings) {
                    startActivityForResult(new Intent(App.getContext(), VOResultActivity.class), R.id.menu_rept_monthly_savings);
                }

        		else if(item.getItemId()==R.id.menu_rept_monthly_loan) {

        		} */
                else if (item.getItemId() == R.id.menu_rept_loan_target_vs_achive) {
                    startActivity(new Intent(App.getContext(), LRTAReportActivity.class));
                } else if (item.getItemId() == R.id.menu_oprtns_smsstat) {
                    startActivity(new Intent(App.getContext(), SMSReportActivity.class));
                }
//        	Toast.makeText(MainActivity_new.this,"You Clicked : " + item.getTitle(),Toast.LENGTH_SHORT).show();
                return true;
            }
        });

        popup.show();
    }

    public void SeasonalLoanSurvey(View v) {
        startActivity(new Intent(App.getContext(), SeasonalSurveyActivity.class));
    }

    public boolean onOptionsItemSelected(MenuItem item) {
//        if (item.getItemId() == R.id.menu_start) {
//            // Start next activity - collector password
//            Startup(null);
//            return true;
//        } else

        if (item.getItemId() == R.id.menu_syncronize) {

            Intent intent = new Intent(getApplicationContext(), SyncDataSimpleJsonParser.class);
            startActivity(intent);
            overridePendingTransition(R.anim.right_in, R.anim.right_out);

            return true;
        } else if (item.getItemId() == R.id.menu_settings) {

            startActivity(new Intent(this, SettingsPermActivity.class));
            overridePendingTransition(R.anim.right_in, R.anim.right_out);

            //startActivity(new Intent(this, SyncSettingsActivity.class));
            return true;
        } else if (item.getItemId() == R.id.menu_video) {
            startActivity(new Intent(this, ShowVideoActivity.class));
            overridePendingTransition(R.anim.right_in, R.anim.right_out);

            return true;
        } else if (item.getItemId() == R.id.menu_datetime) {
            startActivity(new Intent(Settings.ACTION_DATE_SETTINGS));
            overridePendingTransition(R.anim.right_in, R.anim.right_out);

            return true;
        } else if (item.getItemId() == R.id.menu_exit) {
            Exit();
            return true;
        }
/*		else if(item.getItemId() == R.id.menu_without_passbook) {
            startActivity(new Intent(this, WPTransActivity.class));
			return true;
		}
		else if(item.getItemId() == R.id.submenu_wp_entry) {
			startActivity(new Intent(this, WPVOActivity.class));
			return true;
		}
		else if(item.getItemId() == R.id.submenu_wp_verify) {
			Toast.makeText(this, "WP Verify Not yet implemented.", Toast.LENGTH_SHORT).show();
		}
		else if(item.getItemId() == R.id.submenu_wp_edit) {
			Toast.makeText(this, "WP Edit Not yet implemented.", Toast.LENGTH_SHORT).show();
		}
*/
        else if (item.getItemId() == R.id.menu_power) {
            startActivity(new Intent("android.intent.action.POWER_USAGE_SUMMARY"));
            overridePendingTransition(R.anim.right_in, R.anim.right_out);

            return true;
        }
        return false;
    }

    public void Startup(View view) {
        // Button onClick - start activity - collector password
        if (coll.get_Password().length() > 0) {
            Intent it = new Intent(this, LoginActivity.class);
            it.putExtra(LoginActivity.EXTRA_PASS, coll.get_Password());
            // startActivityForResult(it, R.id.menu_start);
        } else {
            Intent it = new Intent(this, TTActivity.class);
            startActivity(it);
        }
    }

    /* (non-Javadoc)
     * @see android.app.Activity#onActivityResult(int, int, android.content.Intent)
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
//            case (R.id.menu_start): {
//                if (resultCode == Activity.RESULT_OK) {
//                    // Start transaction options Activity
//                    Intent it = new Intent(this, TTActivity.class);
//                    startActivity(it);
//                }
//            }
//            break;

            case (R.id.menu_rept_overdue): {
                if (resultCode == Activity.RESULT_OK) {
                    // Overdue report activity
                    Intent it = new Intent(this, ODReportActivity.class);
                    Long vn = data.getLongExtra(P8.VONO, 0);
                    it.putExtra(P8.VONO, vn);
                    startActivity(it);
                }
            }
            break;

            case (R.id.menu_rept_vo_loan_disb): {
                // VO-wise loan disbursement report
                if (resultCode == Activity.RESULT_OK) {
                    Intent it = new Intent(this, VLDReportActivity.class);
                    Long vn = data.getLongExtra(P8.VONO, 0);
                    it.putExtra(P8.VONO, vn);
                    startActivity(it);
                }
            }
            break;

            case (R.id.menu_rept_good_loan): {
                // Good loan list report activity
                if (resultCode == Activity.RESULT_OK) {
                    Intent it = new Intent(this, GoodLoanListActivity.class);
                    Long vn = data.getLongExtra(P8.VONO, 0);
                    it.putExtra(P8.VONO, vn);
                    startActivity(it);
                }
            }
        }
    }

    @Override
    public void SendSMS(String branch_code, String project_code,
                        String po_no, String orgno, String orgmemno, String mobile, String message,
                        final String records) {

        String api_key = "i2yRuU6yspWf7NDsrFdkz7vlMrz35SDZ";
        //encoding message
        String encoded_message = URLEncoder.encode(message);

        //Send SMS API
        String mainUrl = App.getSMSSendURL();

        HashMap<String, String> mParams = new HashMap<String, String>();
        // mParams.put("mobile", mobile.trim());
        mParams.put("mobile", "01685956283");
        mParams.put("branch_code", branch_code);
        mParams.put("orgno", orgno);
        mParams.put("orgmemno", orgmemno);
        mParams.put("message", message);
        mParams.put("po_no", po_no);
        mParams.put("project_code", project_code);
        mParams.put("api_key", api_key);

        final HashMap<String, String> mp = mParams;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, mainUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Display the first 500 characters of the response string.
//						mTextView.setText("Response is: "+ response.substring(0,500));
                        String resp = Html.fromHtml(response).toString().trim();
                        String[] st = resp.split(",");
                        if (st[0].trim().equals("200")) {
                            if (records.trim().length() > 0) {
                                DAO da = new DAO(MainActivity.this);
                                da.open();
                                da.ConfirmSMSSent(records);
                                da.close();
                            }
                        } else {
                            P8.ShowError(MainActivity.this, resp);
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //mTextView.setText("That didn't work!");
                P8.ShowError(MainActivity.this, VolleyErrorResponse(error));
            }
        }) {
            @Override
            public HashMap<String, String> getParams() {
                return mp;
            }
        };

        // Add the request to the RequestQueue.
        CloudRequest.getInstance(this).addToRequestQueue(stringRequest);
    }

    public static String VolleyErrorResponse(VolleyError volleyError) {
        // TODO Auto-generated method stub
        String message = null;
        if (volleyError instanceof NetworkError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof ServerError) {
            message = "The server could not be found. Please try again after some time!!";
        } else if (volleyError instanceof AuthFailureError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof ParseError) {
            message = "Parsing error! Please try again after some time!!";
        } else if (volleyError instanceof TimeoutError) {
            message = "Connection TimeOut! Please check your internet connection.";
        } else {
            message = volleyError.toString();
        }

        try {
            Log.e("myerror", "deal first", volleyError);
            Log.e("myerror", "deal success" + new String(volleyError.networkResponse.data, "utf-8"));
        } catch (Throwable e) {
            Log.e("myerror", "deal fail", e);
        }

        return message;
    }

    @Override
    public void onBackPressed() {
        Exit();
    }

    public void Exit() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Closing Activity")
                .setMessage("Are you sure you want to close application")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }

                })
                .setNegativeButton("No", null)
                .show();
    }

}

