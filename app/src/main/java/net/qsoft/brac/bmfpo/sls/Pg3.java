package net.qsoft.brac.bmfpo.sls;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import net.qsoft.brac.bmfpo.R;
import net.qsoft.brac.bmfpo.data.SurveySL;

import static net.qsoft.brac.bmfpo.sls.SeasonalSurveyActivity.getCheckedValue;
import static net.qsoft.brac.bmfpo.sls.SeasonalSurveyActivity.getRGGetValue;
import static net.qsoft.brac.bmfpo.sls.SeasonalSurveyActivity.setCheckedValue;
import static net.qsoft.brac.bmfpo.sls.SeasonalSurveyActivity.setRGClearAndSet;


public class Pg3 extends android.app.Fragment {

CheckBox chkDL,chkOB,chkPB, chkOS, chkOSI;
    RadioGroup rISource;
    RadioButton MSI;

    public Pg3() {

    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.sls_fragment_pg3, container, false);

        chkDL=(CheckBox) v.findViewById(R.id.chkDL);
        chkOB=(CheckBox) v.findViewById(R.id.chkOB);
        chkPB=(CheckBox) v.findViewById(R.id.chkPB);
        chkOS=(CheckBox) v.findViewById(R.id.chkOS);
        chkOSI =(CheckBox) v.findViewById(R.id.chkOSI);

        rISource=(RadioGroup) v.findViewById(R.id.radioISource);
        MSI=(RadioButton) v.findViewById(R.id.rbMSI);

        MSI.setChecked(true);

        return v;
    }

    public void onResume(){
        super.onResume();
        SurveySL ssl = SurveySL.getLastSurveySL();

        setCheckedValue(chkDL, ssl.getOI_DL());
        setCheckedValue(chkOB, ssl.getOI_OB());
        setCheckedValue(chkPB, ssl.getOI_PB());
        setCheckedValue(chkOS, ssl.getOI_OTHERS());

        setCheckedValue(chkOSI, ssl.getOI_No());

        setRGClearAndSet(rISource, ssl.getOI_ISOURCE());

//        setRGEnabled(rDL, !chkOSI.isChecked());
//        setRGEnabled(rOB, !chkOSI.isChecked());
//        setRGEnabled(rPB, !chkOSI.isChecked());
//        setRGEnabled(rOS, !chkOSI.isChecked());

    }

    @Override
    public void onPause() {
        super.onPause();
        SurveySL ssl = SurveySL.getLastSurveySL();

        ssl.setOI_DL(getCheckedValue(chkDL));
        ssl.setOI_OB(getCheckedValue(chkOB));
        ssl.setOI_PB(getCheckedValue(chkPB));
        ssl.setOI_OTHERS(getCheckedValue(chkOS));

        ssl.setOI_ISOURCE(getRGGetValue(rISource));
        ssl.setOI_No(getCheckedValue(chkOSI));
//        if(ssl.getOI_No()==1) {
//            ssl.setOI_DL(0);
//            ssl.setOI_OB(0);
//            ssl.setOI_PB(0);
//            ssl.setOI_OTHERS(0);
//        }
    }
}