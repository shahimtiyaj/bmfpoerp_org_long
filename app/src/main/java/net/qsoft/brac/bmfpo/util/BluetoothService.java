package net.qsoft.brac.bmfpo.util;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import net.qsoft.brac.bmfpo.P8;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;

public class BluetoothService {

    // Debugging
    private static final String TAG = BluetoothService.class.getSimpleName();
    private static final boolean D = false;

    // Name for the SDP record when creating server socket
    private static final String NAME = "PTP-II";	// "BTPrinter"; //

    // Unique UUID for this application
    private static final UUID MY_UUID = UUID
            .fromString("00001101-0000-1000-8000-00805F9B34FB"); // change by
                                                                    // chongqing
                                                                    // jinou
    // Member fields
    private final BluetoothAdapter mAdapter;
    private final Handler mHandler;
    private AcceptThread mAcceptThread;
    private ConnectThread mConnectThread;
    private ConnectedThread mConnectedThread;
    private int mState;

    // Constants that indicate the current connection state
    public static final int STATE_NONE = 0; // we're doing nothing
    public static final int STATE_LISTEN = 1; // now listening for incoming
                                                // connections
    public static final int STATE_CONNECTING = 2; // now initiating an outgoing
                                                    // connection
    public static final int STATE_CONNECTED = 3; // now connected to a remote
                                                    // device

    private BluetoothDevice mDevice = null;
    private String mDevAddress;
    
    /**
     * Constructor. Prepares a new BTPrinter session.
     * 
     * @param context
     *            The UI Activity Context
     * @param handler
     *            A Handler to send messages back to the UI Activity
     */
    public BluetoothService(Context context, Handler handler) {
        mAdapter = BluetoothAdapter.getDefaultAdapter();
        mState = STATE_NONE;
        mHandler = handler;
        PrepareBluetooth();
    }

    /**
     * Set the current state of the connection
     * 
     * @param state
     *            An integer defining the current connection state
     */
    private synchronized void setState(int state) {
        if (D)
            Log.d(TAG, "setState() " + mState + " -> " + state);
        mState = state;

        // Give the new state to the Handler so the UI Activity can update
        mHandler.obtainMessage(P8.MESSAGE_STATE_CHANGE, state, -1)
                .sendToTarget();
    }

    /**
     * Return the current connection state.
     */
    public synchronized int getState() {
        return mState;
    }

    /**
     * Check Bluetooth service exists
     */
    private void PrepareBluetooth() {
    	 if (mAdapter == null) {
    		 if (D)
    			 Log.d(TAG, "Bluetooth adapter not found");
    		 // Send a failure message back to the Activity
    		 Message msg = mHandler.obtainMessage(P8.MESSAGE_TOAST);
    		 Bundle bundle = new Bundle();
    		 bundle.putString(
	                P8.TOAST,
	                "Bluetooth adapter not found");
	        msg.setData(bundle);
	        mHandler.sendMessage(msg);

    	 } else {
    		 if (D) {
    			 Log.d(TAG, "Bluetooth adapter found");
    			 Log.d(TAG, "Adapter is " + ((mAdapter.isEnabled()) ? "Enabled":"Disabled") );
    		 }
             if(!mAdapter.isEnabled()) 
            	 mAdapter.enable();
        	 Set<BluetoothDevice> mPairedDevices = mAdapter.getBondedDevices();
             if (mPairedDevices.size() > 0) {
                 for (BluetoothDevice mDev : mPairedDevices) {
                	 if (D)
                		 Log.d(TAG, "PairedDevices: " + mDev.getName() + "  "
                             + mDev.getAddress());
                	 if(NAME.equals(mDev.getName())) {
                		 mDevice = mDev;
                		 mDevAddress = mDev.getAddress();
                	 }
                 }
             
//             
//                 Intent enableBtIntent = new Intent(
//                         BluetoothAdapter.ACTION_REQUEST_ENABLE);
//                 startActivityForResult(enableBtIntent,
//                		 BluetoothAdapter.ACTION_REQUEST_ENABLE);
          }
             
    }
             
 }
    
    /**
     * Start the service. Specifically start AcceptThread to begin a session in
     * listening (server) mode. Called by the Activity onResume()
     */
    public synchronized void start() {
    	if (D)
            Log.d(TAG, "start");

        // Cancel any thread attempting to make a connection
        if (mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread = null;
        }

        // Cancel any thread currently running a connection
        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        // Start the thread to listen on a BluetoothServerSocket
/*        if (mAcceptThread == null) {
            mAcceptThread = new AcceptThread();
            mAcceptThread.start();
        } 
        setState(STATE_LISTEN); */
        
        if(mDevice != null)
        	connect(mDevice);
    }

    /**
     * Start the ConnectThread to initiate a connection to a remote device.
     * 
     * @param device
     *            The BluetoothDevice to connect
     */
    public synchronized void connect(BluetoothDevice device) {
        if (D)
            Log.d(TAG, "connect to: " + device);

        // Cancel any thread attempting to make a connection
        if (mState == STATE_CONNECTING) {
            if (mConnectThread != null) {
                mConnectThread.cancel();
                mConnectThread = null;
            }
        }

        // Cancel any thread currently running a connection
        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        // Start the thread to connect with the given device
        mConnectThread = new ConnectThread(device);
        mConnectThread.start();
        setState(STATE_CONNECTING);
    }

    /**
     * Start the ConnectedThread to begin managing a Bluetooth connection
     * 
     * @param socket
     *            The BluetoothSocket on which the connection was made
     * @param device
     *            The BluetoothDevice that has been connected
     */
    public synchronized void connected(BluetoothSocket socket,
            BluetoothDevice device) {
        if (D)
            Log.d(TAG, "connected");

        // Cancel the thread that completed the connection
        if (mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread = null;
        }

        // Cancel any thread currently running a connection
        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        // Cancel the accept thread because we only want to connect to one
        // device
        if (mAcceptThread != null) {
            mAcceptThread.cancel();
            mAcceptThread = null;
        }

        // Start the thread to manage the connection and perform transmissions
        mConnectedThread = new ConnectedThread(socket);
        mConnectedThread.start();

        // Send the name of the connected device back to the UI Activity
        Message msg = mHandler.obtainMessage(P8.MESSAGE_DEVICE_NAME);
        Bundle bundle = new Bundle();
        bundle.putString(P8.DEVICE_NAME, device.getName());
        msg.setData(bundle);
        mHandler.sendMessage(msg);

        setState(STATE_CONNECTED);
    }

    /**
     * Stop all threads
     */
    public synchronized void stop() {
        if (D)
            Log.d(TAG, "stop");
        setState(STATE_NONE);
        if (mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread = null;
        }
        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }
        if (mAcceptThread != null) {
            mAcceptThread.cancel();
            mAcceptThread = null;
        }
//        if(mAdapter.isEnabled())
//        {
//        	mAdapter.disable();
//        }
    }

    /**
     * Write to the ConnectedThread in an unsynchronized manner
     * 
     * @param out
     *            The bytes to write
     * @see ConnectedThread#write(byte[])
     */
    public void write(byte[] out) {
        // Create temporary object
        ConnectedThread r;
        // Synchronize a copy of the ConnectedThread
        synchronized (this) {
            if (mState != STATE_CONNECTED)
                return;
            r = mConnectedThread;
        }
        // Perform the write unsynchronized
        r.write(out);
    }

    public void write(int out) {
        // Create temporary object
        ConnectedThread r;
        // Synchronize a copy of the ConnectedThread
        synchronized (this) {
            if (mState != STATE_CONNECTED)
                return;
            r = mConnectedThread;
        }
        // Perform the write unsynchronized
        r.write(out);
    }
	
    /**
     * Indicate that the connection attempt failed and notify the UI Activity.
     */
    private void connectionFailed() {
        setState(STATE_LISTEN);

        // Send a failure message back to the Activity
        Message msg = mHandler.obtainMessage(P8.MESSAGE_TOAST);
        Bundle bundle = new Bundle();
        bundle.putString(
                P8.TOAST,
                "You can not connect to device, verify that it is on and close to the handheld");
                //"No se puede conectar al dispostivo, verifique que ste se encuentra encendido y cercano a la tablet");
        msg.setData(bundle);
        mHandler.sendMessage(msg);
    }

    /**
     * Indicate that the connection was lost and notify the UI Activity.
     */
    private void connectionLost() {
        // setState(STATE_LISTEN);

        // Send a failure message back to the Activity
        Message msg = mHandler.obtainMessage(P8.MESSAGE_TOAST);
        Bundle bundle = new Bundle();
        bundle.putString(P8.TOAST,
        		"The connection to the device has been lost");
                //"La conexi�n con el dispositivo se ha perdido");
        msg.setData(bundle);
        mHandler.sendMessage(msg);
    }

    public static final byte SMALL = 0x1;
    public static final byte BOLD = 0x8;
    public static final int UNDERLINE = 0x80;
    
    public void DefaultFormat() {
    	byte[] DefaultFMT = { 27, 33, 0 };
    	write(DefaultFMT);
    }

    public void LeftJustify() {
    	byte[] LeftJustify = {27, 97, 48};
    	write(LeftJustify);
    }
    
    public void CenterJustify() {
    	byte[] CenterJustify = {27, 97, 49};
    	write(CenterJustify);
    }
    
    public void RighJustify() {
    	byte[] RightJustify = {27, 97, 50};
    	write(RightJustify);
    }
    
    public void LineSpaceDefault() {
    	byte[] df = {27, 50};
    	write(df);
    }
    
    public void LineSpace(int sp) {
    	byte[] df = {27, 51, 0};
    	df[2] = (byte) sp;
    	write(df);
    }
    
    public void Appearance(int x) {
    	byte[] df = { 27, 33, 0 };
    	df[2] = (byte) x;
    	write(df);
    }
    
    public void ReversePrint(int x) {
    	byte[] df = { 29, 66, 0 };
    	df[2]= (byte) x;
    	write(df);
    }
    
    public void PrinterInit() {
    	byte[] df = {27, 64};
    	write(df);
    }
    
    public void PrintHome() {
    	byte[] df = {27, 60};
    	write(df);
    }
    
    public void LineFeed(int n) {
    	byte[] df = {27, 100, 0};
    	df[2] = (byte) n;
    	write(df);
    }
    
    public void LineFeedRev(int n) {
    	byte[] df = {27, 101, 0};
    	df[2] = (byte) n;
    	write(df);
    }
    
    public void SelectFont(int x) {
      	byte[] df = {27, 77, 0};
    	df[2] = (byte) x;
    	write(df);    	
    }
    
    public void SetBitImageMode() {
    	byte[] df = {0x1B, 0x2A, 33};	//, -128, 0
    	write(df);
    }
    
    /**
     * This thread runs while listening for incoming connections. It behaves
     * like a server-side client. It runs until a connection is accepted (or
     * until cancelled).
     */
    private class AcceptThread extends Thread {
        // The local server socket
        private final BluetoothServerSocket mmServerSocket;

        public AcceptThread() {
            BluetoothServerSocket tmp = null;

            // Create a new listening server socket
            try {
                tmp = mAdapter
                        .listenUsingRfcommWithServiceRecord(NAME, MY_UUID);
            } catch (IOException e) {
                Log.e(TAG, "listen() failed", e);
            }
            mmServerSocket = tmp;
        }

        public void run() {
            if (D)
                Log.d(TAG, "BEGIN mAcceptThread" + this);
            setName("AcceptThread");
            BluetoothSocket socket = null;

            // Listen to the server socket if we're not connected
            while (mState != STATE_CONNECTED) {
                try {
                    // This is a blocking call and will only return on a
                    // successful connection or an exception
                    socket = mmServerSocket.accept();
                } catch (IOException e) {
                    Log.e(TAG, "accept() failed", e);
                    break;
                }

                // If a connection was accepted
                if (socket != null) {
                    synchronized (BluetoothService.this) {
                        switch (mState) {
                        case STATE_LISTEN:
                        case STATE_CONNECTING:
                            // Situation normal. Start the connected thread.
                            connected(socket, socket.getRemoteDevice());
                            break;
                        case STATE_NONE:
                        case STATE_CONNECTED:
                            // Either not ready or already connected. Terminate
                            // new socket.
                            try {
                                socket.close();
                            } catch (IOException e) {
                                Log.e(TAG, "Could not close unwanted socket", e);
                            }
                            break;
                        }
                    }
                }
            }
            if (D)
                Log.i(TAG, "END mAcceptThread");
        }

        public void cancel() {
            if (D)
                Log.d(TAG, "cancel " + this);
            try {
                mmServerSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "close() of server failed", e);
            }
        }
    }

    /**
     * This thread runs while attempting to make an outgoing connection with a
     * device. It runs straight through; the connection either succeeds or
     * fails.
     */
    private class ConnectThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;

        public ConnectThread(BluetoothDevice device) {
            mmDevice = device;
            BluetoothSocket tmp = null;

            // Get a BluetoothSocket for a connection with the
            // given BluetoothDevice
            try {
                tmp = device.createRfcommSocketToServiceRecord(MY_UUID);
            } catch (IOException e) {
                Log.e(TAG, "create() failed", e);
            }
            mmSocket = tmp;
        }

        public void run() {
            Log.i(TAG, "BEGIN mConnectThread");
            setName("ConnectThread");

            // Always cancel discovery because it will slow down a connection
            mAdapter.cancelDiscovery();

            // Make a connection to the BluetoothSocket
            try {
                // This is a blocking call and will only return on a
                // successful connection or an exception
                mmSocket.connect();
            } catch (IOException e) {
                connectionFailed();
                // Close the socket
                try {
                    mmSocket.close();
                } catch (IOException e2) {
                    Log.e(TAG,
                            "unable to close() socket during connection failure",
                            e2);
                }
                // Start the service over to restart listening mode
//                BluetoothService.this.start();
                return;
            }

            // Reset the ConnectThread because we're done
            synchronized (BluetoothService.this) {
                mConnectThread = null;
            }

            // Start the connected thread
            connected(mmSocket, mmDevice);
        }

        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "close() of connect socket failed", e);
            }
        }
    }

    /**
     * This thread runs during a connection with a remote device. It handles all
     * incoming and outgoing transmissions.
     */
    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
//        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket) {
            Log.d(TAG, "create ConnectedThread");
            mmSocket = socket;
//            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the BluetoothSocket input and output streams
            try {
//                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                Log.e(TAG, "temp sockets not created", e);
            }

//            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
//            Log.i(TAG, "BEGIN mConnectedThread");
//            int bytes;
//
//            // Keep listening to the InputStream while connected
//            while (true) {
//                try {
//                    byte[] buffer = new byte[256];
//                    // Read from the InputStream
//                    bytes = mmInStream.read(buffer);
//                    if (bytes > 0) {
//                        // Send the obtained bytes to the UI Activity
//                        mHandler.obtainMessage(P8.MESSAGE_READ, bytes, -1,
//                                buffer).sendToTarget();
//                    } else {
//                        Log.e(TAG, "disconnected");
//                        connectionLost();
//
//                        // add by chongqing jinou
//                        if (mState != STATE_NONE) {
//                            Log.e(TAG, "disconnected");
//                            // Start the service over to restart listening mode
//                            BluetoothService.this.start();
//                        }
//                        break;
//                    }
//                } catch (IOException e) {
//                    Log.e(TAG, "disconnected", e);
//                    connectionLost();
//
//                    // add by chongqing jinou
//                    if (mState != STATE_NONE) {
//                        // Start the service over to restart listening mode
//                        BluetoothService.this.start();
//                    }
//                    break;
//                }
//            }
        }

        /**
         * Write to the connected OutStream.
         * 
         * @param buffer
         *            The bytes to write
         */
        public void write(byte[] buffer) {
            try {
                mmOutStream.write(buffer);
                // Share the sent message back to the UI Activity
                mHandler.obtainMessage(P8.MESSAGE_WRITE, -1, -1, buffer)
                        .sendToTarget();
            } catch (IOException e) {
                Log.e(TAG, "Exception during write", e);
            }
        }

        public void write(int buffer) {
            try {
                mmOutStream.write(buffer);
                // Share the sent message back to the UI Activity
                mHandler.obtainMessage(P8.MESSAGE_WRITE, -1, -1, buffer)
                        .sendToTarget();
            } catch (IOException e) {
                Log.e(TAG, "Exception during write", e);
            }
        }
                
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "close() of connect socket failed", e);
            }
        }
    }
}
