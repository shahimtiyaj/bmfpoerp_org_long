package net.qsoft.brac.bmfpo;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import net.qsoft.brac.bmfpo.data.CLoan;
import net.qsoft.brac.bmfpo.data.CMember;
import net.qsoft.brac.bmfpo.data.DAO;

import java.util.ArrayList;
import java.util.HashMap;

public class WPTransActivity extends SSActivity implements OnItemClickListener, OnItemLongClickListener {
	private static final String TAG = WPTransActivity.class.getSimpleName();

	TextView textTotal;
	Button okButton;
	ListView lv;

	Integer _id=0;

	View vw=null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_wptrans);
		okButton = (Button) findViewById(R.id.okButton);
		okButton.setText("New...");
		textTotal = (TextView) findViewById(R.id.textTotal);

		lv = (ListView) findViewById(R.id.lstView);

		refreshView();
	}


	/* (non-Javadoc)
	 * @see android.app.Activity#onResume()
	 */
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		refreshView();
	}

	private void refreshView() {

		DAO da = new DAO(this);
		da.open();
		ArrayList<HashMap<String, String>> alst = da.getWPPendingTransWithMember();
		Integer mTotal=da.getWPCollectionTotal();
		da.close();

//		Log.d(TAG, "" + alst.size());

		String[] from = new String[] {"[OrgNo]", "[OrgMemNo]", "[MemberName]", "[ColcDate]", "[LoanNo]", "[ColcAmt]" };
        int[] to = { R.id.textVONo, R.id.textMemNo, R.id.textMemName, R.id.textDate, R.id.textLoanNo, R.id.textAmt };

        SimpleAdapter adapter = new SimpleAdapter(getApplicationContext(), alst,
	                R.layout.wptrans_row, from, to);
	    lv.setAdapter(adapter);
		lv.setOnItemClickListener(this);
		lv.setOnItemLongClickListener(this);

		textTotal.setText("Total W.P. Amount: " + mTotal);
	}


	public void onOk(View view) {
		startActivity(new Intent(this, WPVOActivity.class));
	}
	public void onCancel(View view) {
		// Back
		finish();
	}


	@SuppressWarnings("unchecked")
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

		vw = view;

		_id = Integer.parseInt(((HashMap<String, String>) lv.getItemAtPosition(position)).get("[_id]").toString());

		Log.d(TAG, "Id in itemclick: " + _id.toString());

		Long vono = Long.parseLong(((HashMap<String, String>) lv.getItemAtPosition(position)).get("[OrgNo]").toString());
		Long memno = Long.valueOf(((HashMap<String, String>) lv.getItemAtPosition(position)).get("[OrgMemNo]").toString());
		String sln = ((HashMap<String, String>) lv.getItemAtPosition(position)).get("[LoanNo]").toString();

		DAO da = new DAO(this);
		da.open();
		CMember cm = da.getCMember(vono, memno);
		CMember.set_lastCM(cm);
		da.close();

		Intent it = new Intent(this, BCheckResultActivity.class);
		if(sln.equals("Savings")) {
			it.putExtra(P8.TRANS_OPTION, P8.TRANS_OPTION_SAVINGS);
			it.putExtra(P8.BCHECK_BALANCE, CMember.get_lastCM().get_SB());
		}
		else {
			da = new DAO(this);
			da.open();
			CLoan cl = da.getCloan(Integer.parseInt(sln));
			CLoan.set_lastCL(cl);
			da.close();
			it.putExtra(P8.TRANS_OPTION, P8.TRANS_OPTION_REPAYMENT);
			it.putExtra(P8.BCHECK_BALANCE, cl.get_TRB());
		}
		startActivityForResult(it, 2);
	}

	/* (non-Javadoc)
	 * @see android.app.Activity#onActivityResult(int, int, android.content.Intent)
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		Log.d(TAG, "onActivityResult\nrequestCode=" + requestCode + "\nresultCode: " + resultCode
				+ "\nId: " + _id);

		if(requestCode==2)  {
			if(resultCode  == Activity.RESULT_OK) {
				DAO da = null;
				try {
					 da = new DAO(this);
					 da.open();
					 da.addVerifiedWPTransactions(_id);

					 //disable this list item
//					 vw.setEnabled(false);
//					 vw.setOnClickListener(null);

					 // sound success
					 P8.OkSound(this);
					 da.close();
					 Log.d(TAG, "Successfully written");
				} catch (Exception e) {
					 P8.ShowError(this, e.toString());
				}
				finally {
					if(da!=null)
						da.close();
				}

			 }
			 else {

			 }
		 }
	}


	@SuppressWarnings("unchecked")
	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

		vw = view;

		_id = Integer.parseInt(((HashMap<String, String>) lv.getItemAtPosition(position)).get("[_id]").toString());
		long vono = Long.parseLong(((HashMap<String, String>) lv.getItemAtPosition(position)).get("[OrgNo]").toString());
		String memno = ((HashMap<String, String>) lv.getItemAtPosition(position)).get("[OrgMemNo]").toString();
		String sln = ((HashMap<String, String>) lv.getItemAtPosition(position)).get("[LoanNo]").toString();

		DialogInterface.OnClickListener dialogClick = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            	if(which==DialogInterface.BUTTON_POSITIVE) {
            		deleteRecord();
            	}
            	dialog.dismiss();
            }
        };

		P8.ErrorSound(this);
	    AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.title_warrning_dialog)
        	.setMessage("Do you want ot delete this record? \nOrganization: " + vono +
        			"\nMember No.: " + memno +
        			"\nLoan#/Savings: " + sln)
            .setIcon(android.R.drawable.ic_dialog_alert)
            .setCancelable(false)
            .setPositiveButton(android.R.string.yes, dialogClick)
            .setNegativeButton(android.R.string.no, dialogClick).show();

		return true;
	}

	private void deleteRecord() {
		DAO da = null;
		try {
			 da = new DAO(this);
			 da.open();
			 da.deleteWPTransactions(_id);
			 da.close();
			 da=null;
			 //disable this list item
//			 vw.setEnabled(false);
//			 vw.setOnClickListener(null);

			 // sound success
			 refreshView();
			 P8.OkSound(this);
			 
		 } catch (Exception e) {
			 P8.ShowError(this, e.toString());
		 }
		 finally {
			 if(da!=null)
				da.close();
		 }
	}
}
