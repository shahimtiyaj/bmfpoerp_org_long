package net.qsoft.brac.bmfpo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class HDC extends BroadcastReceiver {
	private static final String TAG = HDC.class.getSimpleName();

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		Log.d(TAG, "Intent received: " + intent.getAction());

		if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
			Intent startServiceIntent = new Intent(context, MainActivity_new.class);
			startServiceIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			//context.startService(startServiceIntent);
			context.startActivity(startServiceIntent);
		} else 
			abortBroadcast();
	}
}
