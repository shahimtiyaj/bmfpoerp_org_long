package net.qsoft.brac.bmfpo;

import android.content.Intent;
import android.os.Bundle;

import net.qsoft.brac.bmfpo.data.DAO;

public class WPVOActivity extends VOActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	@Override
	protected void startAction(Integer opt, Long vono) {
		if(opt==2) {
			// Member list
			Intent it = new Intent(this, WPMemberActivity.class);
			it.putExtra(P8.DBITEMS, DAO.DBoardItem.MEMBERS.name());
			it.putExtra(P8.VONO, vono);
			startActivity(it);
		}
	}
}
