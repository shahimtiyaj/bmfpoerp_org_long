package net.qsoft.brac.bmfpo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

public class CListAdapter extends ArrayAdapter<HashMap<String, String>> {
	// View lookup cache
    private static class ViewHolder {
    	TextView gphead;
        TextView memno;
        TextView memname;
        TextView amt;
        TextView type;
    }

	public CListAdapter(Context context, int resource, List<HashMap<String, String>> rows) {
		super(context, resource, rows);
		// TODO Auto-generated constructor stub
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
	       // Get the data item for this position
	       HashMap<String, String> row = getItem(position);    
	       // Check if an existing view is being reused, otherwise inflate the view
	       ViewHolder viewHolder; // view lookup cache stored in tag
	       if (convertView == null) {
	          viewHolder = new ViewHolder();
	          LayoutInflater inflater = LayoutInflater.from(getContext());
	          convertView = inflater.inflate(R.layout.cl_gp_detail, parent, false);
	          viewHolder.gphead = (TextView) convertView.findViewById(R.id.textGPHead);
	          viewHolder.memno = (TextView) convertView.findViewById(R.id.textMemNo);
	          viewHolder.memname = (TextView) convertView.findViewById(R.id.textMemName);
	          viewHolder.amt = (TextView) convertView.findViewById(R.id.textAmt);
	          viewHolder.type = (TextView) convertView.findViewById(R.id.textType);
	          
	          convertView.setTag(viewHolder);
	       } else {
	           viewHolder = (ViewHolder) convertView.getTag();
	       }
//	       if(position%2==1)
//	    	   convertView.setBackgroundColor(Color.CYAN);
//	       else
//	    	   convertView.setBackgroundColor(Color.TRANSPARENT);
	       
	       // Populate the data into the template view using the data object
	       if(row.get("VOName").isEmpty()) {
	    	   viewHolder.gphead.setVisibility(View.GONE);
	       }
	       else {
	    	   viewHolder.gphead.setVisibility(View.VISIBLE);
	    	   viewHolder.gphead.setText(row.get("VONo") + " - " + row.get("VOName"));
	       }
	       
	       viewHolder.memno.setText(row.get("MemNo"));
	       viewHolder.memname.setText(row.get("MemName"));
	       viewHolder.amt.setText(row.get("Amt"));
	       viewHolder.type.setText(row.get("Type"));
	       
	       // Return the completed view to render on screen
	       return convertView;
	   }	
}
